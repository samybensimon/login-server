const express = require('express');
const bodyParser = require('body-parser');
let jwt = require('jsonwebtoken');
let cors = require('cors');
let config = require('./config');
let middleware = require('./middleware');

class HandlerGenerator {
    login(req, res) {
        let username = req.body.username;
        let password = req.body.password;
        // For the given username fetch user from DB
        let mockedUsername = 'samy';
        let mockedPassword = '1';

        if (username && password) {
            if (username === mockedUsername && password === mockedPassword) {
                let token = jwt.sign({ username: username },
                    config.secret,
                    {
                        expiresIn: '24h' // expires in 24 hours
                    }
                );
                // return the JWT token for the future API calls
                console.log('logged ! => ', token)
                res.json({
                    success: true,
                    message: 'Authentication successful!',
                    token: token
                });
            } else {
                res.json({
                    status: 403,
                    success: false,
                    message: 'Incorrect username or password'
                });
            }
        } else {
            res.json({
                status: 400,
                success: false,
                message: 'Authentication failed! Please check the request'
            });
        }
    }
    getTokenForResetPassword(req, res, next) {
        let token = jwt.sign({ username: req.body.username },
            config.secret,
            {
                expiresIn: 30//900 // expires in 15 minutes
            }
        );
        let urlForResetPassword = `http://localhost:4200/new-password/${token}`;
        console.log(urlForResetPassword);
        res.json({ token: token, success: true, message: 'Un email a bien été envoyé' })
    }

    updatePassword(req, res, next) {
        let newPassword = req.body.passwordHashed;
        let username = req.body.username;
        console.log('Update new password')
    }
    isTokenExpired(req, res, next) {
        jwt.verify(req.body.token, config.secret, (err, decoded) => {
            if (err) {

                return res.json({
                    success: false
                });
            } else {
                return res.json({
                    success: true
                });
            }
        });
    }

    index(req, res) {
        res.json({
            success: true,
            message: 'Index page'
        });
    }
}

// Starting point of the server
function main() {
    let app = express(); // Export app for other routes to use
    let handlers = new HandlerGenerator();
    const port = process.env.PORT || 8000;
    app.use(bodyParser.urlencoded({ // Middleware
        extended: true
    }));
    app.use(cors())
    app.options('*', cors())
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.use(bodyParser.json());
    // Routes & Handlers
    app.post('/login', handlers.login); // loggin
    app.post('/getTokenForResetPassword', handlers.getTokenForResetPassword); // send email with token available during 15mn
    app.put('/updatePassword', handlers.updatePassword); // update password
    app.post('/check-if-token-is-expired', handlers.isTokenExpired); // update password
    app.get('/', middleware.checkToken, handlers.index); // test if token is expired or not
    app.listen(port, () => console.log(`Server is listening on port: ${port}`));
}

main();